<?php
require('app/init.php');
if (!empty($_POST)) {
    $email = $_POST['email'];
    $user = $userHelper->findUserByEmail($email);
    if ($user) {
        $tokenData = $tokenHandler->createForgotPasswordToken($user->id);
        if ($tokenData) {
            $mail->addAddress($user->email);
            $mail->Subject = "Password Recovery!";
            $mail->Body = "Use this link within 10 minutes to reset the password: <br>";
            $mail->Body .= "<a href='http://localhost:9999/reset_password.php?t={$tokenData['token']}'>Reset Password</a>";
            if ($mail->send()) {
                echo "Please check your email to reset password!";
            } else {
                echo "Problem sending mail, try again later!";
            }
        } else {
            echo "<h3>Issue is server, try again later!</h3>";
        }
    } else {
        echo "<h3>no such email found!</h3>";
    }
} else if ($auth->check()) {
    echo "<h3>u are already signed in!";
} else {
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Forgot Password</title>
    </head>

    <body>
        <h1>Forgot Password</h1>
        <form action="forgot_password.php" method="POST">
            <fieldset>
                <legend>Forgot Password</legend>
                <label>
                    Email: <br>
                    <input type="email" name="email">
                </label>
                <br><br>
                <input type="submit" value="Reset Password" name="submit">
            </fieldset>
        </form>
    </body>

    </html>

<?php
}
?>