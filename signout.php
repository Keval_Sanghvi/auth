<?php
require "app/init.php";

if ($auth->check()) {
    $user = $auth->user();
    $auth->signout();
    //  delete token from db
    $tokenHandler->deleteToken($user->id, 1);
    // clear token cookie from browser history
    unset($_COOKIE['token']);
    setcookie('token', '', time() - 3600);

    header('Location: index.php');
} else {
    echo "Unauthorized access!";
}
