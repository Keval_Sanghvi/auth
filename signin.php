<?php
require "app/init.php";
if (!empty($_POST)) {
    $username = $_POST['username'];
    $password = $_POST['password'];
    $rememberMe = $_POST['rem'] ?? null;
    $status = $auth->signin($username, $password);
    if ($status) {
        if ($rememberMe) {
            $user = $userHelper->findUserByUsername($username);
            $token = $tokenHandler->createRememberMeToken($user->id);
            setcookie("token", $token['token'], time() + 1800);
        }
        header('Location: index.php');
    } else {
        echo "Invalid Username or Password!";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign In</title>
</head>

<body>
    <?php if (isset($_COOKIE['token']) && $tokenHandler->isValid($_COOKIE['token'], 1)) : ?>
        <h3>Your are already signed in!</h3>
    <?php else : ?>
        <h1>Login</h1>
        <form action="signin.php" method="POST">
            <fieldset>
                <legend>Sign In</legend>
                <label>
                    Username:
                    <input type="text" name="username">
                </label>
                <br>
                <br>
                <label>
                    Password:
                    <input type="password" name="password">
                </label>
                <br>
                <br>
                <label>
                    <input type="checkbox" name="rem" checked> Remember Me
                </label>
                <br>
                <br>
                <input type="submit" name="submit" value="Sign In">
            </fieldset>
        </form>
    <?php endif; ?>
</body>

</html>