<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

class MailConfigHelper
{
    public static function getMailer(int $debugMode = 0): PHPMailer
    {
        $mail = new PHPMailer();

        $mail->SMTPDebug = $debugMode;
        $mail->isSMTP();
        $mail->Host = 'smtp.mailtrap.io';
        $mail->Port = 2525;
        $mail->SMTPAuth = true;
        $mail->Username = '270d11a36af39f';
        $mail->Password = '76cced21ce7250';
        $mail->SMTPSecure = 'tls';
        $mail->isHtml(true);
        $mail->setFrom('admin@keval.com', 'Keval Team');

        return $mail;
    }
}
