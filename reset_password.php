<?php
require 'app/init.php';
if (!empty($_GET['t'])) {
    $token = $_GET['t'];
    if ($token && $tokenHandler->isValid($token, 0)) :
?>
        <form action="reset_password.php" method="POST">
            <fieldset>
                <legend>Reset Password</legend>
                <label>
                    New Password:
                    <input type="password" name="password">
                </label>
                <br><br>
                <input type="hidden" name="t" value="<?= $token; ?>">
                <input type="submit" value="Reset Password" name="submit">
            </fieldset>
        </form>
<?php
    else :
        echo "<h3>Invalid Link, try with authenticated link!</h3>";
    endif;
}
// submit form
else if (!empty($_POST)) {
    $password = $_POST['password'];
    $token = $_POST['t'];
    if ($tokenHandler->isValid($token, 0)) {
        $user = $userHelper->findUserByToken($token);
        if ($auth->updatePassword($user->id, $password)) {
            $tokenHandler->deleteTokenByToken($token);
            echo "<h3>Password reset successfully!</h3>";
            echo "<br><a href='signin.php'>Sign In</a>";
        } else {
            echo "Problem with server while updating password, try again later!";
        }
    } else {
        echo "Timeout, generate new link!";
    }
} else {
    echo "Looks like a fishy activity!";
}
